Установка с PyPI
------------------------------

::

    pip3 install --user alpha-dbw


Установка из исходников
------------------------------

::

    git clone https://gitlab.com/starline/alpha_py.git && cd alpha_py
    pip3 install --user -e .
