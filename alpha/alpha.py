#!/usr/bin/env python

#############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import sys
import time
import json
import threading
import traceback

from math import cos, sin, tan, pi

from .interface import SerialVehicleInterface
from .protocol  import Protocol, VEHICLE_MODE
from .loops     import Timer
from .log       import Logger
from .asp       import print_raw

from .control import *

from . import log


STATE_CHECK_RATE = 2
CONTROLLER_RATE  = 50
ODOMETRY_RATE    = 50
LOG_RATE         = 50

LOG_DIR = "/tmp/alpha_logs"

ALPHA_PORT      = "/dev/ALPHA"
ALPHA_TEST_PORT = "/dev/ALPHA_TEST_PC_SIDE"

DEFAULT_TAKEOVER_ID = 'DIRECT'

try:
    import alpha_ws as AWS
    HAS_AWS = True
except:
    HAS_AWS = False


class CONTROL_TYPE():

    RAW         = 0x01
    CONTROLLER  = 0x02
    PLATFORM    = 0x03

    from_str = {'raw'        : 0x01,
                'controller' : 0x02,
                'platform'   : 0x03}

    human_readable = { RAW        : 'raw',
                       CONTROLLER : 'controller',
                       PLATFORM   : 'platform'}


class VehicleParams(dict):
    """
    Класс для задания всех параметров используемого автомобиля. Наследуется от dict.
    Соответственно, параметры задаются как ключи словаря.

    :param wheel_radius:       Радиус колес автомобиля [м]
    :param wheel_width:        Ширина колес автомобиля [м]
    :param wheelbase:          Осевое расстояние между колес [м]
    :param axle_track:         Межосевое расстояние [м]
    :param steering_ratio:     Передаточное число между углом поворота руля и направляющей угла поворота колес
    :param max_sw_angle:       Максимальный угол поворота руля [град]
    :param max_sw_rate:        Максимальная скорость вращения руля [град/с]
    :param max_acceleration:   Максимальное ускорение [м/с^2]
    :param max_deceleration:   Максимальное торможение (положительное значение) [м/с^2]
    :param control_type:       Способ управления автомобилем [raw, controller, platform]
    :param controller_name:    Наименование контроллера автомобиля
    :param controller_rate:    Частота расчета управления для автомобиля
    :param controller_params:  Параметры контроллера автомобиля
    :param odometry_rate:      Частота расчета одометрии автомобиля
    """

    def __init__(self, *args, **kwargs):

        self["alpha_port"]        = ALPHA_PORT

        self["wheel_radius"]      = 0.37
        self["wheel_width"]       = 0.25
        self["wheelbase"]         = 2.75
        self["axle_track"]        = 1.64
        self["steering_ratio"]    = 14.8
        self["max_sw_angle"]      = 500
        self["max_sw_rate"]       = 250
        self["max_acceleration"]  = 2.0
        self["max_deceleration"]  = 6.
        self["odometry_rate"]     = ODOMETRY_RATE

        self["control_type"]      = "controller"
        self["controller_name"]   = "SwPid"
        self["controller_rate"]   = CONTROLLER_RATE

        controller = available_controllers()[self["controller_name"]]
        self["controller_params"] = controller.Params()

        self["sensors_power_supply"] = {}

        super(VehicleParams, self).__init__(*args, **kwargs)


class WSCfg(dict):

    def __init__(self, *args, **kwargs):

        self['port']      = None

        self['instream']  = {'control' : {'cb'   : Vehicle._ws_in_control_cb}}
        self['outstream'] = {'state'   : {'cb'   : Vehicle._ws_out_state_cb,
                                          'rate' : 50}}

        self['get']       = {'state'  : {'cb' : Vehicle._ws_get_state_cb},
                             'params' : {'cb' : Vehicle._ws_get_params_cb},
                             'powers' : {'cb' : Vehicle._ws_get_powers_cb}}

        self['set']       = {'powers'      : {'cb' : Vehicle._ws_set_powers_cb},
                             'takeover'    : {'cb' : Vehicle._ws_set_takeover_cb},
                             'params'      : {'cb' : Vehicle._ws_set_params_cb},
                             'save_params' : {'cb' : Vehicle._ws_set_save_params_cb},
                             'estop'       : {'cb' : Vehicle._ws_set_estop_cb}}

        super(WSCfg, self).__init__(*args, **kwargs)


class _Singleton(object):

    _instance = None

    def __new__(class_, *args, **kwargs):

        if not isinstance(class_._instance, class_):
            class_._instance = object.__new__(class_)

        return class_._instance


class Vehicle(_Singleton):
    """
    Основной класс для взаимодействия с автомобилем.

    :param params: Объект с параметрами автомобиля
    :type params: VehicleParams, dict or None
    :param ws_cfg: Конфигурация для alpha_ws сервера
    :type params: WSCfg or None
    """

    def __init__(self, params = None, ws_cfg = None, logger = None):

        self.logger = logger or Logger()

        self._protocol = Protocol(self.logger)

        self._control_type = CONTROL_TYPE.RAW

        self._vehicle_mode = VEHICLE_MODE.UNKNOWN

        self._sensors_power_state = {}

        self._odometry = Odometry(self)

        self._vehicle_log     = None
        self._vehicle_log_dir = LOG_DIR

        self._takeover_id = None
        self._takeover_ev = threading.Event()

        self._takeover_try_ev = threading.Event()
        self._takeover_src_l  = threading.Lock()

        # -------------------------------------------------

        self._params = VehicleParams(params) if params else VehicleParams(params)

        self.set_control_type(self._params["control_type"])

        self._control_tm = Timer(self._calc_control,
                                 self._params["controller_rate"])

        self.set_controller(self._params["controller_name"],
                            self._params["controller_params"])

        self._interface = SerialVehicleInterface(self._protocol,
                                                 self._params["alpha_port"],
                                                 self._interface_connect_cb,
                                                 self._interface_disconnect_cb)

        Timer(self._check_vehicle_state_cb, STATE_CHECK_RATE).start()

        self._odometry_tm = Timer(self._odometry.calc_odometry,
                                  self._params["odometry_rate"])

        # self._vehicle_log_tm = Timer(self._log_vehicle_state, LOG_RATE)

        # aws ---------------------------------------------

        self._ws_server = None
        self._ws_cfg    = None

        if ws_cfg:
            self.start_websocket_iface(ws_cfg)


    # INTERFACE ---------------------------------------------------------- #

    def _interface_connect_cb(self):
        pass


    def _interface_disconnect_cb(self):
        pass


    def interface_connected(self):
        return self._interface.connected


    # CONTROLLER --------------------------------------------------------- #

    def start_controller(self):
        if self._controller:
            self._controller.reset()
            self._control_tm.start()


    def stop_controller(self):
        self._control_tm.stop()


    def set_controller(self, controller_name, params = None):

        if (controller_name in available_controllers() and
            not self._control_tm.is_active()):

            self._controller = new_controller(controller_name, self, params)

            self._params["controller_name"]   = controller_name
            self._params["controller_params"] = self._controller.get_params()

        else:
            print('Error: there is no such controller ' + str(controller_name))


    def set_controller_params(self, params):

        if self._controller:
            self._controller.update_params(params)


    def set_controller_rate(self, rate):
        if self._control_tm.set_rate(rate):
            self._params["controller_rate"] = rate


    def get_actual_controller_rate(self):
        return self._control_tm.get_real_rate()


    def _calc_control(self):

        throttle, sw_torque = self._controller.calc_output()

        self._protocol.set_vehicle_throttle(throttle)
        self._protocol.set_sw_torque(sw_torque)


    # CONTROL ------------------------------------------------------------ #

    def set_control_type(self, type):

        if (self._vehicle_mode in (VEHICLE_MODE.DRIVE, VEHICLE_MODE.REVERSE) or
            not type in CONTROL_TYPE.from_str):

            return False

        else:
            self._control_type = CONTROL_TYPE.from_str[type]
            self._params["control_type"] = type
            return True


    def move(self, throttle = None, speed = None, acceleration = None, jerk = None):
        """
        Задает желаемые значения движения автомобиля.
        Учет параметров speed, acceleration и jerk происходит только при
        использовании контроллера (см. Control).

        :param throttle: Ускорение/торможение автомобиля в процентах [-100:100]
        :type throttle: float
        :param speed: Скорость автомобиля (контроллер) [м/с]
        :type speed: float
        :param acceleration: Ускорение автомобиля (контроллер) [м/с^2]
        :type acceleration: float
        :param jerk: Рывок автомобиля (контроллер) [м/с^3]
        :type jerk: float
        """

        if self._control_type == CONTROL_TYPE.CONTROLLER:
            self._controller.set_target_move(speed, acceleration, jerk, throttle)

        else:
            self._protocol.set_vehicle_throttle(throttle)


    def steer(self,  angle = None, velocity = None, torque = None):
        """
        Задает желаемые значения вращения руля автомобиля.
        Учет параметров angle, velocity происходит только при
        использовании контроллера (см. Control).

        :param angle: Угол поворота руля (контроллер) [град]
        :type angle: float
        :param velocity: Скорость вращения руля (контроллер) [град/с]
        :type velocity: float
        :param torque: Момент, прикладываемый к рулю в процентах [-100:100]
        :type torque: float
        """

        if self._control_type == CONTROL_TYPE.CONTROLLER:
            self._controller.set_target_steering(angle, velocity, torque)

        elif self._control_type == CONTROL_TYPE.RAW:
            self._protocol.set_sw_torque(torque)

        elif self._control_type == CONTROL_TYPE.PLATFORM:
            self._protocol.set_sw_angle_and_velocity(angle, velocity)


    def get_vehicle_speed(self):
        """
        Возвращает текущее значение скорости автомобиля в м/с

        :rtype: float
        """

        return self._protocol.get_vehicle_speed()


    def get_vehicle_acceleration(self):
        return self._protocol.get_vehicle_acceleration()


    def get_gas_pedal(self):
        return self._protocol.get_gas_pedal()


    def get_brake_pedal(self):
        return self._protocol.get_brake_pedal()


    def get_steering_wheel_angle(self):
        """
        Возвращает текущий угол поворота руля [град]

        :rtype: float
        """

        return self._protocol.get_sw_angle()


    def get_steering_wheel_velocity(self):
        """
        Возвращает текущую скорость вращения руля [град/с]

        :rtype: float
        """

        return self._protocol.get_sw_velocity()


    def get_steering_wheel_angle_and_velocity(self):
        return self._protocol.get_sw_angle_and_velocity()


    def get_steering_wheel_torque_cmd(self):
        """
        Возвращает результат расчета контроллера руля по моменту [%]

        :rtype: float
        """

        return self._protocol.sw_torque_cmd.last_torque_cmd


    def get_steering_wheel_and_eps_torques(self):
        return self._protocol.get_sw_and_eps_torques()


    def start_send_vehicle_cmd(self):
        """
        Начинает потоковую отправку команд для движения и управления рулем автомобиля
        """

        self._protocol.start_sending_vehicle_move_cmd()
        self._protocol.start_sending_sw_torque_cmd()


    def stop_send_vehicle_cmd(self):
        """
        Останавливает потоковую отправку команд для движения и управления рулем
        автомобиля
        """

        self._protocol.stop_sending_vehicle_move_cmd()
        self._protocol.stop_sending_sw_torque_cmd()

        self._protocol.set_vehicle_throttle(0)
        self._protocol.set_sw_torque(0)


    def start_sending_vehicle_move_cmd(self):
        return self._protocol.start_sending_vehicle_move_cmd()


    def stop_sending_vehicle_move_cmd(self):
        self._protocol.stop_sending_vehicle_move_cmd()
        self._protocol.set_vehicle_throttle(0)


    def start_sending_steering_wheel_torque_cmd(self):
        return self._protocol.start_sending_sw_torque_cmd()


    def stop_sending_sw_torque_cmd(self):
        self._protocol.stop_sending_sw_torque_cmd()
        self._protocol.set_sw_torque(0)


    # MODE --------------------------------------------------------------- #

    def get_mode(self):
        return self._vehicle_mode


    def get_takeover_id(self):
        return self._takeover_id


    def drive(self, takeover_id = DEFAULT_TAKEOVER_ID, control_type = None):
        return self.takeover(VEHICLE_MODE.DRIVE, takeover_id, control_type)


    def reverse(self, takeover_id = DEFAULT_TAKEOVER_ID, control_type = None):
        return self.takeover(VEHICLE_MODE.REVERSE, takeover_id, control_type)


    def takeover(self, vehicle_mode,
                       takeover_id = DEFAULT_TAKEOVER_ID,
                       control_type = None):

        if self._takeover_try_ev.is_set():
            return False

        if not vehicle_mode in (VEHICLE_MODE.DRIVE, VEHICLE_MODE.REVERSE):
            return False

        with self._takeover_src_l:

            on_takeover = self._vehicle_mode in (VEHICLE_MODE.DRIVE,
                                                 VEHICLE_MODE.REVERSE)

            if on_takeover:

                if self._takeover_id != takeover_id:
                    return False

            else:

                if not self._takeover_id:
                    self._takeover_id = takeover_id

                if control_type:

                    if control_type in CONTROL_TYPE.from_str:
                        self._control_type = CONTROL_TYPE.from_str[control_type]
                    else:
                        return False

                self._takeover_try_ev.set()

                threading.Thread(target = self._takeover_wait_cb,
                                 kwargs = {"timeout": 1.5},
                                 daemon = True).start()

            # -------------------------------------------------

            if vehicle_mode == VEHICLE_MODE.DRIVE:
                return self._protocol.drive_mode()

            else:
                return self._protocol.reverse_mode()


    def _takeover_wait_cb(self, timeout):

        if not self._takeover_ev.wait(timeout):

            self._takeover_id = None
            self._control_type = CONTROL_TYPE.from_str[self._params["control_type"]]

            self.manual()

        self._takeover_try_ev.clear()


    def manual(self):
        return self._protocol.manual_mode()


    def _check_vehicle_state_cb(self):

        self._takeover_src_l.acquire()

        vehicle_mode, source = self._protocol.get_mode()

        if self._vehicle_mode != vehicle_mode:

            if vehicle_mode in (VEHICLE_MODE.DRIVE, VEHICLE_MODE.REVERSE):

                if source == self._protocol.launcher_info.SOURCE_BUTTON:

                    self._takeover_id = DEFAULT_TAKEOVER_ID
                    self._control_type = CONTROL_TYPE.from_str[self._params["control_type"]]

                elif source == self._protocol.launcher_info.SOURCE_CMD:

                    if not self._takeover_id:
                        self._takeover_id = DEFAULT_TAKEOVER_ID

                if self._control_type == CONTROL_TYPE.CONTROLLER:
                    self.start_controller()

                self.start_send_vehicle_cmd()

                self._takeover_ev.set()

            elif vehicle_mode == VEHICLE_MODE.MANUAL:

                self._takeover_ev.clear()

                self.stop_send_vehicle_cmd()
                self.stop_controller()

                self._takeover_id  = None
                self._control_type = CONTROL_TYPE.from_str[self._params["control_type"]]

            self._vehicle_mode = vehicle_mode

        self._takeover_src_l.release()

        # self._sensors_power_state = self.get_sensors_power_supply()


    # STOPs -------------------------------------------------------------- #

    def emergency_stop_on(self):
        """
        Включает режим экстренного торможения

        :rtype: Bool
        """

        return self._protocol.emergency_stop_on()


    def emergency_stop_off(self):
        """
        Выключает режим экстренного торможения

        :rtype: Bool
        """

        return self._protocol.emergency_stop_off()


    def recover(self):
        return (self._protocol.hand_brake_off() and
                self._protocol.emergency_stop_off())


    def hand_brake_on(self):
        """
        Активирует ручной тормоз

        :rtype: Bool
        """

        return self._protocol.hand_brake_on()


    def hand_brake_off(self):
        """
        Снимает ручной тормоз

        :rtype: Bool
        """

        return self._protocol.hand_brake_off()


    def get_emergency_stop(self):
        """
        Возвращает текущее состояние режима экстренного торможения

        :rtype: Bool
        """
        return self._protocol.get_emergency_stop()


    def get_hand_brake(self):
        """
        Возвращает текущее состояние ручного тормоза

        :rtype: Bool
        """

        return self._protocol.get_hand_brake()


    # LIGHTS ------------------------------------------------------------- #

    def led_blink(self):
        return self._protocol.led_blink()


    def led_on(self):
        return self._protocol.led_on()


    def led_off(self):
        return self._protocol.led_off()


    def get_led(self):
        return self._protocol.get_led()


    def left_turn_signal(self):
        """
        Включает левый сигнал поворота

        """

        self._protocol.left_turn_signal()


    def right_turn_signal(self):
        """
        Включает правый сигнал поворота

        """

        self._protocol.right_turn_signal()


    def emergency_signals(self):
        """
        Включает аварийную сигнализацию

        """

        self._protocol.emergency_signals()


    def turn_off_signals(self):
        """
        Выключает сигналы поворота и аварийную сигнализацию

        """

        self._protocol.turn_off_signals()


    # PERIPHERY ---------------------------------------------------------- #

    def set_sensors_power_supply(self, sensors):

        bitmask = self._sensors_power_state_to_bitmask(sensors)
        return self._protocol.set_sensors_power_supply(bitmask)


    def get_sensors_power_supply(self):

        bitmask = self._protocol.get_sensors_power_supply()
        return self._sensor_power_bitmask_to_states(bitmask)


    def _sensors_power_state_to_bitmask(self, sensors):

        bitmask = 0

        for sensor in sensors:

            if not sensor in self._params["sensors_power_supply"]:
                continue

            if not sensors[sensor]:
                continue

            bitmask |= 1 << (self._params["sensors_power_supply"][sensor] - 1)

        return bitmask


    def _sensor_power_bitmask_to_states(self, bitmask):

        sensors_poses = self._params["sensors_power_supply"]

        states = {}

        for sensor in sensors_poses:

            state = bool((1 << (sensors_poses[sensor] - 1)) & bitmask)

            states.update({sensor : state})

        return states


    # -----------------------------------------------------

    def sensors_clear_toggle(self):
        return self._protocol.sensors_clear_toggle()


    # LOGGER ------------------------------------------------------------- #

    # def _log_vehicle_state(self):
    #     cur_time = time.time()

    #     mode = self._vehicle_mode

    #     vehicle_speed         = self._protocol.get_vehicle_speed()
    #     sw_angle, sw_velocity = self._protocol.get_sw_angle_and_velocity()
    #     sw_torque, eps_torque = self._protocol.get_sw_and_eps_torques()

    #     self._vehicle_log.add_data(cur_time,
    #                                sw_angle,
    #                                sw_velocity,
    #                                sw_torque,
    #                                eps_torque,
    #                                vehicle_speed,
    #                                mode)


    # def start_vehicle_logger(self):
    #     self._vehicle_log = log.VehicleLog(self._vehicle_log_dir)
    #     self._vehicle_log_tm.start()


    # def change_vehicle_logger_dir(self, log_dir):
    #     self._vehicle_log_dir = log_dir


    # def stop_vehicle_logger(self):
    #     self._vehicle_log_tm.stop()
    #     self._vehicle_log.save()
    #     self._vehicle_log = None


    # def error_report(self):
    #     return 'NO_ERROR'


    # WEBSOCKET IFACE ---------------------------------------------------- #

    def start_websocket_iface(self, cfg = None):

        if not HAS_AWS:
            return

        if self._ws_server:
            return

        if cfg:
            self._ws_cfg = cfg

        if not self._ws_cfg:
            return

        self._ws_server = AWS.Server(self._ws_cfg['port'],
                                     self._websocket_connect_cb,
                                     self._websocket_disconnect_cb)

        for tag in self._ws_cfg['outstream']:

            self._ws_server.stream(self._ws_cfg['outstream'][tag]['cb'],
                                   self._ws_cfg['outstream'][tag]['rate'],
                                   tag)

        for tag in self._ws_cfg['instream']:
            self._ws_server.subscribe(self._ws_cfg['instream'][tag]['cb'], tag)

        for tag in self._ws_cfg['get']:
            self._ws_server.bind_on_get(self._ws_cfg['get'][tag]['cb'], tag)

        for tag in self._ws_cfg['set']:
            self._ws_server.bind_on_set(self._ws_cfg['set'][tag]['cb'], tag)

        self._ws_server.up()


    def stop_websocket_iface(self):

        if self._ws_server:
            self._ws_server.down()
            self._ws_server = None


    def pp_websocket_connections(self):

        if self._ws_server:
            print(json.dumps(self._ws_server.get_connections(), indent=2))


    def _websocket_connect_cb(self, cl_handler):
        pass


    def _websocket_disconnect_cb(self, cl_handler):
        pass


    @classmethod
    def _ws_in_control_cb(cls, msg):

        vehicle = cls._instance

        if vehicle.get_mode() not in (VEHICLE_MODE.DRIVE, VEHICLE_MODE.REVERSE):
            return

        if vehicle._takeover_id == msg['data']['takeover_id']:

            if vehicle._control_type == CONTROL_TYPE.RAW:
                vehicle.steer(torque = msg['data']['sw_target'])
            else:
                vehicle.steer(angle = msg['data']['sw_target'])

            vehicle.move(throttle = msg['data']['throttle'])


    @classmethod
    def _ws_out_state_cb(cls):

        vehicle = cls._instance

        sw_angle, sw_rate = vehicle.get_steering_wheel_angle_and_velocity()

        return {'iface_connected' : vehicle._interface.connected,
                'mode'            : vehicle.get_mode(),
                'velocity'        : vehicle.get_vehicle_speed(),
                'sw_angle'        : sw_angle,
                'sw_rate'         : sw_rate,
                'sw_torque'       : 0,
                'gas'             : vehicle.get_gas_pedal(),
                'brake'           : vehicle.get_brake_pedal(),
                'hand_brake'      : vehicle.get_hand_brake(),
                'estop'           : vehicle.get_emergency_stop(),
                'led'             : vehicle.get_led(),
                'takeover_id'     : vehicle._takeover_id,
                'control_type'    : vehicle._control_type}


    @classmethod
    def _ws_get_state_cb(cls, msg):
        return cls._instance._ws_out_state_cb()


    @classmethod
    def _ws_get_params_cb(cls, msg):
        return cls._instance._params


    @classmethod
    def _ws_set_params(cls, params, params_to_fill):

        for name in params:

            if not name in params_to_fill:
                continue

            if isinstance(params[name], dict):
                cls._ws_set_params(params[name], params_to_fill[name])

            else:
                params_to_fill[name] = params[name]


    @classmethod
    def _ws_set_params_cb(cls, msg):

        vehicle = cls._instance

        new_params = msg['data']

        if 'controller_name' in new_params:
            vehicle.set_controller(new_params['controller_name'])
            new_params.pop('controller_name')

        if 'controller_params' in new_params:
            vehicle.set_controller_params(new_params['controller_params'])
            new_params.pop('controller_params')

        if 'control_type' in new_params:
            vehicle.set_control_type(new_params['control_type'])
            new_params.pop('control_type')

        if 'controller_rate' in new_params:
            vehicle.set_controller_rate(new_params['controller_rate'])
            new_params.pop('controller_rate')

        if 'odometry_rate' in new_params:
            vehicle.set_odometry_calc_rate(new_params['odometry_rate'])
            new_params.pop('odometry_rate')

        vehicle._ws_set_params(new_params, vehicle._params)

        return True


    @classmethod
    def _ws_set_save_params_cb(cls, msg):
        return True
        # return cls._instance._params


    @classmethod
    def _ws_get_powers_cb(cls, msg):
        return cls._instance.get_sensors_power_supply()


    @classmethod
    def _ws_set_powers_cb(cls, msg):
        return cls._instance.set_sensors_power_supply(msg['data'])


    @classmethod
    def _ws_set_takeover_cb(cls, msg):

        vehicle = cls._instance

        cur_mode = vehicle.get_mode()

        on_takeover = cur_mode in (VEHICLE_MODE.DRIVE, VEHICLE_MODE.REVERSE)

        mode = msg['data']['mode']
        type = msg['data']['control_type']
        tid  = msg['data']['takeover_id']

        if mode == VEHICLE_MODE.MANUAL:
            vehicle.manual()
            return True

        vehicle.takeover(mode, tid, type)

        return True

        # if tid and not on_takeover:
        #     AWS.Alarm(vehicle._ws_set_takeover_alarm_cb, 1.5, tid)


    # def _ws_set_takeover_alarm_cb(self, takeover_id):
    #
    #     if self._takeover_id == takeover_id:
    #
    #         mode = self.get_mode()
    #
    #         if mode not in (VEHICLE_MODE.DRIVE, VEHICLE_MODE.REVERSE):
    #             self.manual()


    @classmethod
    def _ws_set_estop_cb(cls, msg):

        vehicle = cls._instance

        if msg['data']['value']:
            vehicle.emergency_stop_on()
        else:
            vehicle.recover()

        return True



    # Odometry ----------------------------------------------------------------

    def start_odometry_calculation(self):
        if self._odometry_tm.is_active():
            self._odometry_tm.stop()
            self._odometry.reset()
        self._odometry_tm.start()


    def stop_odometry_calculation(self):
        self._odometry_tm.stop()


    def get_odometry(self):
        return self._odometry.get()


    def reset_odometry(self):
        self._odometry.reset()


    def set_odometry_calc_rate(self, rate):

        if self._odometry_tm.set_rate(rate):
            self._params["odometry_rate"] = rate


    def get_actual_odometry_calc_rate(self):
        return self._odometry_tm.get_real_rate()


class Odometry():

    def __init__(self, vehicle):

        self._vehicle = vehicle

        self._pose_data_lock = threading.Lock()

        self.x    = 0.0
        self.y    = 0.0
        self.yaw  = 0.0
        self.dx   = 0.0
        self.dy   = 0.0
        self.dyaw = 0.0
        self.time = 0.0


    def get(self):
        return self.x, self.y, self.yaw, self.dx, self.dy, self.dyaw, self.time


    def reset(self):
        with self._pose_data_lock:
            self.x    = 0.0
            self.y    = 0.0
            self.yaw  = 0.0
            self.dx   = 0.0
            self.dy   = 0.0
            self.dyaw = 0.0
            self.time = 0.0


    def calc_odometry(self):

        cur_time = time.time()
        vehicle_velocity      = self._vehicle.get_vehicle_speed()
        sw_angle, sw_velocity = self._vehicle.get_sw_angle_and_velocity()

        wheelbase      = self._vehicle._params["wheelbase"]
        steering_ratio = self._vehicle._params["steering_ratio"]

        vehicle_steering_tan = tan((sw_angle * pi / 180) / steering_ratio)

        with self._pose_data_lock:

            dt = cur_time - self.time

            self.dx = cos(self.yaw) * vehicle_velocity
            self.dy = sin(self.yaw) * vehicle_velocity

            self.dyaw = vehicle_velocity / wheelbase * vehicle_steering_tan

            self.x   += dt * self.dx
            self.y   += dt * self.dy
            self.yaw += dt * self.dyaw

            self.time = cur_time


def spin():

    while True:

        try:
            time.sleep(2)

        except KeyboardInterrupt:
            break
