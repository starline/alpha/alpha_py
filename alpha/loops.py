#!/usr/bin/env python

#############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

from threading import Thread, Event
from time      import sleep, time


class StoppableThread(Thread):

    def __init__(self,  target = None, *args, **kwargs):

        super(StoppableThread, self).__init__(target = target,
                                              *args,
                                              **kwargs)

        self._stop_event = Event()
        self._stopped    = Event()
        self.daemon      = True

        self._stopped.set()


    def stop(self):
        self._stop_event.set()


    def stopped(self):
        return self._stopped.is_set()


    def running(self):
        return not (self._stop_event.is_set() or self._stopped.is_set())


    def run(self):

        self._stopped.clear()
        self._stop_event.clear()

        if self._target:
            self._target(*self._args, **self._kwargs)

        self._stopped.set()


    def wait_for_stop(self, timeout = None):
        return self._stopped.wait(timeout)


class Timer():

    def __init__(self, target = None, rate = 1.0):
        self._delay  = 1. / abs(rate)
        self._target = target
        self._thread = None
        self.real_rate = 0.0


    def _rated_loop(self, *args, **kwargs):

        next_call_time = time()

        while self._thread.running():

            next_call_time = next_call_time + self._delay

            self._target(*args, **kwargs)

            rest_time = next_call_time - time()

            # self.real_rate = 0 if (rest_time <= 0) else (1. / rest_time)

            sleep(max(rest_time, 0))


    def set_target(self, target):
        self.stop()
        self._target = target


    def set_rate(self, rate):

        if rate > 0 and isinstance(rate, (int, float)):
            self._delay  = 1. / abs(rate)
            return True

        else:
            return False


    def is_active(self):
        return (self._thread and self._thread.is_alive())


    def is_not_active(self):
        return not self.is_active()


    def start(self,  *args, **kwargs):
        if self.is_not_active():
            self._thread = StoppableThread(target = self._rated_loop,
                                             args = args,
                                           kwargs = kwargs,
                                           daemon = True)

            self._thread.start()


    def stop(self):
        if self.is_active():
            self._thread.stop()


    def wait_for_stop(self, timeout = 1):
        return self._thread.wait_for_stop(timeout)


    def restart(self):
        self.stop()
        self.start()


    def get_real_rate(self):
        return self.real_rate
