#!/usr/bin/env python

#############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import inspect
import sys
import threading
import time

from time import time
from math import cos, sin, tan, copysign, exp, inf

from . import log


def available_controllers():
    """
    Возвращает словарь с доступными контроллерами
    Ключ - наименование контроллера
    Значение - соответствующий класс контроллера

    :rtype: dict
    """

    controllers = {}
    for name, cls in inspect.getmembers(sys.modules[__name__]):
        if inspect.isclass(cls) and name != "BaseController":
            controllers.update({name: cls})

    return controllers


def new_controller(controller_name, vehicle, params = None):
    """
    Возвращает объект контроллера для автомобиля или None в случае
    невозможности найти контроллер с заданным наименованием

    :param controller_name: наименование контроллера
    :type controller_name: str
    :param vehicle: объект автомобиля, используется для получения актуальных
    показаний с автомобиля для реализации управления
    :type vehicle: Vehicle
    :param params: параметры контроллера
    :type params: dict
    """

    if controller_name:
        controllers = available_controllers()
        if controller_name in controllers:
            return controllers[controller_name](vehicle, params)
        else:
            log.print_warn("There is no " + str(controller_name) + " controller!")

    return None


class BaseController(object):
    """
    Базовый клас контроллера. Не реализует управления и служит для основы для
    создания всех контроллеров, работающих в рамках API. Подкласс Params определяет
    параметры для контроллера по умолчанию.

    :param vehicle: объект автомобиля, используется для получения актуальных
    показаний с автомобиля для реализации управления
    :type vehicle: Vehicle
    :param params: параметры контроллера
    :type params: BaseController.Params
    """

    MAX_DELAY_IN_CONTROL_LOOP = 0.2

    class Params(dict):

        def __init__(self, *args, **kwargs):

            super(BaseController.Params, self).__init__(*args, **kwargs)


    def __init__(self, vehicle, params = None):

        self._params = self.Params(params) if params else self.Params()

        self._vehicle = vehicle

        self._target_vehicle_speed        = None
        self._target_vehicle_acceleration = None
        self._target_vehicle_jerk         = None
        self._target_vehicle_throttle     = None

        self._target_sw_angle    = None
        self._target_sw_velocity = None
        self._target_sw_torque   = None

        self._output_vehicle_throttle = None
        self._output_sw_torque        = None

        self._last_calc_ts = 0


    def update_params(self, params):
        self._params.update(params)


    def get_params(self):
        return self._params


    def reset(self):
        self._last_calc_ts = 0


    def set_target_move(self, speed        = None,
                              acceleration = None,
                              jerk         = None,
                              throttle     = None):

        self._target_vehicle_speed        = speed
        self._target_vehicle_acceleration = acceleration
        self._target_vehicle_jerk         = jerk
        self._target_vehicle_throttle     = throttle


    def set_target_steering(self, angle    = None,
                                  velocity = None,
                                  torque   = None):

        self._target_sw_angle    = angle
        self._target_sw_velocity = velocity
        self._target_sw_torque   = torque


    def _calc_throttle_output(self, d_time):
        pass


    def _calc_sw_torque_output(self, d_time):
        pass


    def calc_output(self):

        cur_calc_ts = time()
        d_time = cur_calc_ts - self._last_calc_ts

        self._last_calc_ts = cur_calc_ts

        if (d_time > self.MAX_DELAY_IN_CONTROL_LOOP):

            warn_msg = 'control calc rate is too low: ' + str(round(1. / d_time, 2))
            self._vehicle.logger.push('warn', warn_msg)

            return 0.0, 0.0

        self._calc_sw_torque_output(d_time)
        self._calc_throttle_output(d_time)

        return self._output_vehicle_throttle, self._output_sw_torque


class SwPid(BaseController):

    class Params(dict):

        def __init__(self, *args, **kwargs):

            self["P"]              = 0.1
            self["I"]              = 0.1
            self["I_saturation"]   = 80.
            self["D"]              = 0.001
            self["out_saturation"] = 90.

            super(SwPid.Params, self).__init__(*args, **kwargs)


    def __init__(self, vehicle, params = None):

        super(SwPid, self).__init__(vehicle, params)

        self._target_sw_angle    = 0.0
        self._target_sw_velocity = 0.0

        self._target_vehicle_throttle = 0.0

        self._output_vehicle_throttle = 0.0
        self._output_sw_torque        = 0.0

        self._last_sw_angle_error = 0.0
        self._last_sw_angle       = 0.0

        self._p_term = 0.0
        self._i_term = 0.0
        self._d_term = 0.0


    def reset(self):

        super(SwPid, self).reset()

        self._last_sw_angle_error = 0.0
        self._target_sw_angle     = self._vehicle.get_steering_wheel_angle()
        self._i_term = 0.0


    def _calc_throttle_output(self, d_time):
        self._output_vehicle_throttle = clamp(self._target_vehicle_throttle,
                                              -100, 100)


    def _calc_sw_torque_output(self, d_time):

        # cur_vehicle_speed = self._vehicle.get_vehicle_speed()
        self._last_sw_angle = self._vehicle.get_steering_wheel_angle()

        sw_angle_error = self._target_sw_angle - self._last_sw_angle

        self.p_term = sw_angle_error * self._params['P']

        new_i_term = self._i_term + sw_angle_error * d_time * self._params['I']
        if abs(new_i_term) <  self._params['I_saturation']:
            self._i_term = new_i_term

        d_sw_angle_error = sw_angle_error - self._last_sw_angle_error

        self._last_sw_angle_error = sw_angle_error

        self.d_term = (d_sw_angle_error / d_time) * self._params['D']

        output_sw_torque = self.p_term + self._i_term + self.d_term

        if abs(output_sw_torque) <  self._params['out_saturation']:
            self._output_sw_torque = output_sw_torque
        else:
            self._output_sw_torque = copysign(self._params['out_saturation'],
                                              output_sw_torque)


    def calc_output(self):

        super(SwPid, self).calc_output()

        event_data = {'calc_ts' : self._last_calc_ts,
                      'name'    : str(self.__class__.__name__),
                      'params'  : self._params,

                      'targets' : {'sw_angle' : self._target_sw_angle,
                                   'throttle' : self._target_vehicle_throttle},

                      'state'   : {'p_term'   : self._p_term,
                                   'i_term'   : self._i_term,
                                   'd_term'   : self._d_term,
                                   'sw_angle' : self._last_sw_angle,
                                   'error'    : self._last_sw_angle_error},

                      'outputs' : {'sw_torque' : self._output_sw_torque,
                                   'throttle'  : self._output_vehicle_throttle}
                      }

        self._vehicle.logger.push('event/controller', event_data)

        return self._output_vehicle_throttle, self._output_sw_torque


class SwPidVelRanges(SwPid):

    class Params(SwPid.Params):

        def __init__(self, *args, **kwargs):

            self['vel_ranges'] = []
            self['vel_step']   = 1.0

            super(SwPidVelRanges.Params, self).__init__(*args, **kwargs)


    def __init__(self, vehicle, params = None):

        super(SwPidVelRanges, self).__init__(vehicle, params)

        self._vrange = None
        self._vranges = []

        self._rparams = {}

        self._update_params_l = threading.Lock()

        self._last_velocity = self._vehicle.get_vehicle_speed()

        self._check_vranges()


    def reset(self):
        super(SwPidVelRanges, self).reset()
        self._vrange = None


    def update_params(self, params):

        with self._update_params_l:

            super(SwPidVelRanges, self).update_params(params)
            self._check_vranges()


    def _pick_vrange(self):

        for vrange in self._vranges:

            lower = vrange['lower'] if 'lower' in vrange else -inf
            upper = vrange['upper'] if 'upper' in vrange else  inf

            if lower <= self._last_velocity and self._last_velocity <= upper:

                self._rparams = vrange
                return

        # -------------------------------------------------

        self._rparams = self._params


    def _check_vranges(self):

        self._vranges = []

        if not 'vel_ranges' in self._params:
            return

        # -------------------------------------------------

        for vrange in self._params['vel_ranges']:

            cur_lower = vrange['lower'] if 'lower' in vrange else -inf
            cur_upper = vrange['upper'] if 'upper' in vrange else  inf

            if cur_lower > cur_upper:

                print('[ALPHA]: Controller vel range lower is bigger ' +
                      'then upper! Default main params is used.')

                return

            # comparison to neighbor ranges ---------------

            for nhbr_vrange in self._params['vel_ranges']:

                if nhbr_vrange is vrange:
                    continue

                nhbr_lower = nhbr_vrange['lower'] if 'lower' in vrange else -inf
                nhbr_upper = nhbr_vrange['upper'] if 'upper' in vrange else  inf

                if (cur_lower > nhbr_lower and cur_upper < nhbr_upper or
                    cur_lower == -inf and nhbr_lower == -inf          or
                    cur_upper ==  inf and nhbr_upper ==  inf):

                    print('[ALPHA]: Controller vel ranges has ' +
                        'intersections! Default main params is used.')

                    return

        self._vranges = self._params['vel_ranges']
        self._pick_vrange()


    def _calc_sw_torque_output(self, d_time):

        cur_velocity = self._vehicle.get_vehicle_speed()
        cur_sw_angle = self._vehicle.get_steering_wheel_angle()

        if abs(self._last_velocity - cur_velocity) > self._params['vel_step']:

            self._last_velocity = cur_velocity

            with self._update_params_l:
                self._pick_vrange()

        sw_angle_error = self._target_sw_angle - cur_sw_angle

        p_term = sw_angle_error * self._rparams['P']

        new_i_term = self._i_term + sw_angle_error * d_time * self._rparams['I']
        if abs(new_i_term) <  self._rparams['I_saturation']:
            self._i_term = new_i_term

        d_sw_angle_error = sw_angle_error - self._last_sw_angle_error
        self._last_sw_angle_error = sw_angle_error

        d_term = (d_sw_angle_error / d_time) * self._rparams['D']

        output_sw_torque = p_term + self._i_term + d_term

        if abs(output_sw_torque) <  self._rparams['out_saturation']:
            self._output_sw_torque = output_sw_torque
        else:
            self._output_sw_torque = copysign(self._rparams['out_saturation'], output_sw_torque)


class SwPidInEWMA(SwPid):

    class Params(SwPid.Params):

        def __init__(self, *args, **kwargs):

            self["frequency"] = 10

            super(SwPidInEWMA.Params, self).__init__(*args, **kwargs)


    def __init__(self, vehicle, params = None):

        super(SwPidInEWMA, self).__init__(vehicle, params)
        self._last_EWMA = 0.0


    def reset(self):
        super(SwPidInEWMA, self).reset()
        self._last_EWMA = self._target_sw_angle


    def set_target_steering(self, angle    = None,
                                  velocity = None,
                                  torque   = None):

        cur_time = time()
        d_time = cur_time - self._last_control_time
        self._last_control_time = cur_time

        alpha = 1 - exp((-1.0 * d_time) * self._params["frequency"])

        EWMA = alpha * angle + (1 - alpha) * self._last_EWMA

        self._last_EWMA = self._target_sw_angle

        self._target_sw_angle    = EWMA
        self._target_sw_velocity = velocity
        self._target_sw_torque   = torque


# helpers ------------------------------------------

def clamp(value, min_value, max_value):
    return max(min_value, min(value, max_value))


def sign(value):
    return 1 if value >= 0 else -1
