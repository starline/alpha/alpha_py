### Alpha

Репозиторий содержит API низкоуровневой системы управления автомобилем [Alpha](https://alpha.starline.ru), разрабатываемой в рамках проекта [OSCAR](https://gitlab.com/starline/oscar), для беспилотных транспортных средств.


#### Установка с PyPI

```
pip3 install --user alpha-dbw
```


#### Установка из исходников

```
git clone https://gitlab.com/starline/alpha_py.git && cd alpha_py
pip3 install --user -e .
```


#### Использование

```
import alpha
vehicle = alpha.Vehicle()
vehicle.drive()
vehicle.steer(20)
vehicle.move(10)
vehicle.manual()
vehicle.led_blink()
vehicle.emergency_stop()
vehicle.recover()
vehicle.left_turn_signal()
vehicle.right_turn_signal()
vehicle.emergency_signals()
vehicle.turn_off_signals()
vehicle.get_vehicle_speed()
```

#### udev-правила для Alpha

Для удобства работы с последовательным портом можно создать udev-правила,
для этого перейдите в дерикторию alpha_py и выполните:

```
bash scripts/udev_rules/create_udev.bash
```

После этого, при подключении сигмы-ретранслятора к ПК, для взаимодействия
с Alpha будет создаваться последовательный порт ```/dev/alpha```.


#### Виртуальный порт

Требуется установка `socat`:

```
sudo apt install socat
```

Для создания и удаления виртуальных портов доступны два скрипта:

```
scripts/virtual_serial_up.bash  
scripts/virtual_serial_down.bash
```

После выполнения первого скрипта в `/dev` создадуться два связанных порта:

```
/dev/ALPHA_TEST_PC_SIDE         - порт для использования пакетом alpha
/dev/ALPHA_TEST_VEHICLE_SIDE    - порт для имитации платформы и отладки
```
